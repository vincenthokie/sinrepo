from django.shortcuts import render
from django.http import Http404
from django.views import View

from django.db import connection

import django.core.serializers
from django.shortcuts import redirect
from django.forms.models import model_to_dict
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from django.core.urlresolvers import reverse

from adminApp.models import CapsuleBrand, Drugs, Symptom, Manufacturer, AdminInstruction, DrugPrecaution, DrugSideeffect, DrugOverdose, Beverage, Food, Disease, SymptomTreatment, DrugToBeverage, DrugToDisease, DrugToDrug, DrugToFood, Country, Blister, Orientation, BodyPart, Colors, Brand, DrugSvgImage
from django.contrib.auth.models import User

from adminApp.forms import (
	ManufacturerForm, 
	DrugForm, 
	BodyPartForm, 
	SymptomForm, 
	SymptomTreatmentForm, 
	AdministrationInstructionForm,
	DrugPrecautionsForm,
	FoodForm,
	CapsuleForm,
	DiseaseForm,
	DrugToDrugForm,
	DrugSideeffectForm,
	DrugPrecautionsForm,
	DrugToBeverageForm,
	BeverageForm,
	DrugToFoodForm,
	DrugOverdoseForm,
	DrugToDiseaseForm,
	)

from django.contrib.auth.mixins import LoginRequiredMixin

'''form = NewBusinessForm(request.POST)
    if form.is_valid():
        # process form data
        obj = Listing() #gets new object
        obj.business_name = form.cleaned_data['business_name']
        obj.business_email = form.cleaned_data['business_email']
        obj.business_phone = form.cleaned_data['business_phone']
        obj.business_website = form.cleaned_data['business_website']
        #finally save the object in db
        obj.save()
        '''


def index(request):
	return render(  request, 'admin/pages/view/dashboard.html' )


class Dashboard(LoginRequiredMixin, View):
	
	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return render(  request, 'admin/pages/view/dashboard.html', {
			'CapsuleBrand': CapsuleBrand.objects.all().count(),
			'Drugs': Drugs.objects.all().count(),
			'Symptom': Symptom.objects.all().count(),
			'Manufacturer': Manufacturer.objects.all().count(),
			'AdminInstruction': AdminInstruction.objects.all().count(),
			'DrugPrecaution': DrugPrecaution.objects.all().count(),
			'DrugSideeffect': DrugSideeffect.objects.all().count(),
			'DrugOverdose': DrugOverdose.objects.all().count(),
			'Beverage': Beverage.objects.all().count(),
			'Food': Food.objects.all().count(),
			'Disease': Disease.objects.all().count(),
			'SymptomTreatment': SymptomTreatment.objects.all().count(),
			'DrugToBeverage': DrugToBeverage.objects.all().count(),
			'DrugToDisease': DrugToDisease.objects.all().count(),
			'DrugToDrug': DrugToDrug.objects.all().count(),
			'DrugToFood': DrugToFood.objects.all().count(),
			})


class CountryView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return render(  request, 'admin/pages/view/viewcountry.html',{
			"countries" : Country.objects.all()
			})


class ManufacturerView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/view/addmanufacturer.html'
	context = { "mans" : Manufacturer.objects.all() }

	def get(self, request, *args, **kwargs):
		self.context["form"] = ManufacturerForm()
		self.context["success"] = False
		self.context["success_delete"] = False

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = ManufacturerForm(request.POST)
		if form.is_valid():
		# process form data
			obj = Manufacturer() #gets new object
			obj.man_name = form.cleaned_data['man_name']
			obj.save()

			if obj.manufacturer_id is not None:
				self.context["success"] = "good"
				self.context["form"] = ManufacturerForm()
				self.context["mans"] = Manufacturer.objects.all()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class DrugView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/view/adddrug.html'
	context = { "drugs" : Drugs.objects.all() }

	def get(self, request, *args, **kwargs):
		self.context["form"] = DrugForm()
		self.context["success"] = False
		self.context["success_delete"] = False

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = DrugForm(request.POST)
		if form.is_valid():
		# process form data
			obj = Drugs() #gets new object
			obj.drug_name = form.cleaned_data['drug_name']
			obj.save()

			if obj.drug_id is not None:
				self.context["success"] = "good"
				self.context["form"] = DrugForm()
				self.context["drugs"] = Drugs.objects.all()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class BodyPartView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/view/addbodypart.html'
	context = { "parts" : BodyPart.objects.all() }

	def get(self, request, *args, **kwargs):
		self.context["form"] = BodyPartForm()
		self.context["success"] = False
		self.context["success_delete"] = False

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = BodyPartForm(request.POST)
		if form.is_valid():
		# process form data
			obj = BodyPart() #gets new object
			obj.body_part = form.cleaned_data['body_part']
			obj.save()

			if obj.part_id is not None:
				self.context["success"] = "good"
				self.context["form"] = BodyPartForm()
				self.context["parts"] = BodyPart.objects.all()
			else:
				self.context["success"] = "bad"
				self.context["form"] = BodyPartForm(request.POST)

			return render(  request, self.url , self.context  )


class SymptomView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/view/addsymptom.html'
	context = { "parts" : BodyPart.objects.all()  }

	def get(self, request, *args, **kwargs):
		#symptoms = Symptom.objects.select_related('part_id')
		#symptoms = symptoms.query

		self.context["symptoms"] = self.get_symptoms()

		self.context["form"] = SymptomForm()
		self.context["success"] = False
		self.context["success_delete"] = False

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"

		return render(  request, self.url , self.context  )


	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#symptoms = Symptom.objects.select_related('part_id')
		#symptoms = symptoms.query

		form = SymptomForm(request.POST)
		if form.is_valid():
		# process form data
			obj = Symptom() #gets new object
			obj.symptom = form.cleaned_data['symptom']
			obj.part_id = form.cleaned_data['part_id']
			obj.description = form.cleaned_data['description']
			obj.save()

			if obj.symptom_id is not None:
				self.context["success"] = "good"
				self.context["form"] = SymptomForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			self.context["symptoms"] = self.get_symptoms()
			return render(  request, self.url , self.context  )


	def get_symptoms(self):
		cursor = connection.cursor()
		cursor.execute("SELECT `symptom`.`symptom_id`, `symptom`.`symptom`, `symptom`.`description`, `body_part`.`part_id`, `body_part`.`body_part` FROM `symptom` \
			JOIN `body_part` ON (`symptom`.`part_id` = `body_part`.`part_id`)")
		return cursor.fetchall()


class SymptomTreatmentView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/view/addsymptomcure.html'
	context = { "symptoms" : Symptom.objects.all(), "brands" : Brand.objects.all() }

	def get(self, request, *args, **kwargs):
		#cures = SymptomTreatment.objects.select_related('symptom_id', 'brand_id')
		#cures = cures.query

		self.context["cures"] = self.get_symptom_cures()
		self.context["form"] = SymptomTreatmentForm()
		self.context["success"] = False
		self.context["success_delete"] = False

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#cures = SymptomTreatment.objects.select_related('symptom_id', 'brand_id')
		#cures = cures.query

		form = SymptomTreatmentForm(request.POST)
		if form.is_valid():
		# process form data
			obj = SymptomTreatment() #gets new object
			obj.symptom_id = form.cleaned_data['symptom_id']
			obj.brand_id = form.cleaned_data['brand_id']
			obj.save()

			if obj.treatment_id is not None:
				self.context["success"] = "good"
				self.context["form"] = SymptomTreatmentForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form
				
			self.context["cures"] = self.get_symptom_cures()
			return render(  request, self.url , self.context  )


	def get_symptom_cures(self):
		cursor = connection.cursor()
		cursor.execute("SELECT `symptom_treatment`.`treatment_id`, `symptom_treatment`.`symptom_id`, `symptom_treatment`.`brand_id`, `symptom`.`symptom`, `brand`.`brand_name` \
			FROM `symptom_treatment` \
			JOIN `symptom` ON (`symptom_treatment`.`symptom_id` = `symptom`.`symptom_id`) \
			JOIN `brand` ON (`symptom_treatment`.`brand_id` = `brand`.`brand_id`)")
		return cursor.fetchall()


class AdministrationInstructionView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/view/admininstruction.html'
	context = { "drugs" : Drugs.objects.all() }

	def get(self, request, *args, **kwargs):
		#cures = AdminInstruction.objects.select_related('drug_id')
		#cures = cures.query

		self.context["instructions"] = self.get_instructions()

		self.context["form"] = AdministrationInstructionForm()
		self.context["success"] = False
		self.context["success_delete"] = False

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#cures = AdminInstruction.objects.select_related('drug_id')
		#cures = cures.query

		form = AdministrationInstructionForm(request.POST)
		if form.is_valid():
		# process form data
			obj = AdminInstruction() #gets new object
			obj.drug_id = form.cleaned_data['drug_id']
			obj.instructions = form.cleaned_data['instructions']
			obj.save()

			if obj.instruction_id is not None:
				self.context["success"] = "good"
				self.context["form"] = AdministrationInstructionForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form
				
			self.context["instructions"] = self.get_instructions()
			return render(  request, self.url , self.context  )


	def get_instructions(self):
		cursor = connection.cursor()
		cursor.execute("SELECT `admin_instruction`.`instruction_id`, `admin_instruction`.`instructions`, `drugs`.`drug_name`, `drugs`.`drug_id` \
			FROM `admin_instruction` \
			JOIN `drugs` ON (`admin_instruction`.`drug_id` = `drugs`.`drug_id`) ")
		return cursor.fetchall()


class CapsulesView(LoginRequiredMixin, View):

	login_url = '/admin/login/'
	url = 'admin/pages/view/viewcapsule.html'
	context = {}
	context["success_delete"] = False

	def get(self, request, *args, **kwargs):
		#capsules = CapsuleBrand.objects.select_related( 'manufacturer_id', 'drug_id', 'country_id', 'orientation_id' )
		#capsules = capsules.query

		deleted = request.session.pop('success_delete', False)
		if deleted:
			if deleted == "good":
				self.context["success_delete"] = "good"
			else:
				self.context["success_delete"] = "bad"
		
		capsules = self.get_capsules()
		alist = self.get_colors(capsules)

		self.context["capsules"] = zip(capsules, alist)
		self.context["form"] = CapsuleForm()

		return render(  request, self.url , self.context  )


	def post(self, request, *args, **kwargs):
		return render(  request, self.url , self.context  )


	def get_capsules(self):
		cursor = connection.cursor()
		cursor.execute("SELECT `capsule_brand`.`brand_id`, `capsule_brand`.`brand_name`, `capsule_brand`.`color1`, `capsule_brand`.`color2`, `capsule_brand`.`noCpsOnBlister`, `drugs`.`drug_id`, `drugs`.`drug_name`, `manufacturer`.`man_name`, `country`.`nicename` \
			FROM `capsule_brand` \
			JOIN `drugs` ON (`capsule_brand`.`drug_id` = `drugs`.`drug_id`) \
			JOIN `manufacturer` ON (`capsule_brand`.`manufacturer_id` = `manufacturer`.`manufacturer_id`) \
			JOIN `country` ON (`capsule_brand`.`country_id` = `country`.`country_id`)")
		return cursor.fetchall()

	def get_colors(self, solution):
		alist = []
		for sol in solution:
			innerList = []
			innerList.append(  returnSVG(  Colors.objects.get(  color_id=sol[2]  ).color, Colors.objects.get(  color_id=sol[3]  ).color  )  )
			alist.append( innerList )
		return alist











class DrugBrandSVGView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/adddrugsvgs.html'
	context = {}

	def get(self, request, *args, **kwargs):
		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		
		myfile = request.FILES['drugSvgs']
		fs = FileSystemStorage()
		myfile.name = myfile.name.replace(" ", "_").replace("..", "_")
		filename = fs.save('drug_svgs/'+myfile.name, myfile)

		if fs.url(filename):
			obj = DrugSvgImage()
			obj.file_name = myfile.name
			obj.save()

			response = JsonResponse({
				'status' : 'OK',
				#'path' : fs.url(filename),
				'fname' : myfile.name
				})

			if obj.svg_id is not None:
				return response


class AddManufacturerView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/addmanufacturer.html'
	context = {}

	def get(self, request, *args, **kwargs):
		self.context["form"] = ManufacturerForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = ManufacturerForm(request.POST)
		if form.is_valid():
		# process form data
			obj = Manufacturer() #gets new object
			obj.man_name = form.cleaned_data['man_name']
			obj.save()

			if obj.manufacturer_id is not None:
				self.context["success"] = "good"
				self.context["form"] = ManufacturerForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class AddDrugView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/adddrug.html'
	context = {}

	def get(self, request, *args, **kwargs):
		self.context["form"] = DrugForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = DrugForm(request.POST)
		if form.is_valid():
		# process form data
			obj = Drugs() #gets new object
			obj.drug_name = form.cleaned_data['drug_name']
			obj.save()

			if obj.drug_id is not None:
				self.context["success"] = "good"
				self.context["form"] = DrugForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class AddBodyPartView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	context = {}
	url = 'admin/pages/add/addbodypart.html'

	def get(self, request, *args, **kwargs):
		self.context["form"] = BodyPartForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = BodyPartForm(request.POST)
		if form.is_valid():
		# process form data
			obj = BodyPart() #gets new object
			obj.body_part = form.cleaned_data['body_part']
			obj.save()

			if obj.part_id is not None:
				self.context["success"] = "good"
				self.context["form"] = BodyPartForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = BodyPartForm(request.POST)

			return render(  request, self.url , self.context  )


class AddSymptomView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/addsymptom.html'
	context = { "parts" : BodyPart.objects.all()  }

	def get(self, request, *args, **kwargs):
		#symptoms = Symptom.objects.select_related('part_id')
		#symptoms = symptoms.query

		self.context["form"] = SymptomForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )


	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#symptoms = Symptom.objects.select_related('part_id')
		#symptoms = symptoms.query

		form = SymptomForm(request.POST)
		if form.is_valid():
		# process form data
			obj = Symptom() #gets new object
			obj.symptom = form.cleaned_data['symptom']
			obj.part_id = form.cleaned_data['part_id']
			obj.description = form.cleaned_data['description']
			obj.save()

			if obj.symptom_id is not None:
				self.context["success"] = "good"
				self.context["form"] = SymptomForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class AddSymptomTreatmentView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/addsymptomcure.html'
	context = { "symptoms" : Symptom.objects.all(), "brands" : Brand.objects.all() }

	def get(self, request, *args, **kwargs):
		#cures = SymptomTreatment.objects.select_related('symptom_id', 'brand_id')
		#cures = cures.query

		self.context["form"] = SymptomTreatmentForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#cures = SymptomTreatment.objects.select_related('symptom_id', 'brand_id')
		#cures = cures.query

		form = SymptomTreatmentForm(request.POST)
		if form.is_valid():
		# process form data
			obj = SymptomTreatment() #gets new object
			obj.symptom_id = form.cleaned_data['symptom_id']
			obj.brand_id = form.cleaned_data['brand_id']
			obj.save()

			if obj.treatment_id is not None:
				self.context["success"] = "good"
				self.context["form"] = SymptomTreatmentForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class AddAdministrationInstructionView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/admininstruction.html'
	context = { "drugs" : Drugs.objects.all() }

	def get(self, request, *args, **kwargs):
		#cures = AdminInstruction.objects.select_related('drug_id')
		#cures = cures.query

		self.context["form"] = AdministrationInstructionForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#cures = AdminInstruction.objects.select_related('drug_id')
		#cures = cures.query

		form = AdministrationInstructionForm(request.POST)
		if form.is_valid():
		# process form data
			obj = AdminInstruction() #gets new object
			obj.drug_id = form.cleaned_data['drug_id']
			obj.instructions = form.cleaned_data['instructions']
			obj.save()

			if obj.instruction_id is not None:
				self.context["success"] = "good"
				self.context["form"] = AdministrationInstructionForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )


class AddCapsulesView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/add/addcapsule.html'
	context = {}

	def get(self, request, *args, **kwargs):

		self.context["form"] = CapsuleForm()
		self.context["success"] = "x"

		return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):

		form = CapsuleForm(request.POST)

		if form.is_valid():
		# process form data
 
			obj = CapsuleBrand() #gets new object

			obj.drug_id = form.cleaned_data['drug_id']
			obj.brand_name = form.cleaned_data['brand_name']
			obj.manufacturer_id = form.cleaned_data['manufacturer_id']
			obj.country_id = form.cleaned_data['country_id']
			obj.nocpsonblister = form.cleaned_data['nocpsonblister']
			obj.color1 = Colors.objects.get(  color=form.cleaned_data['noIndColor1']  ).color_id     
			obj.color2 = Colors.objects.get(  color=form.cleaned_data['noIndColor2']  ).color_id
			obj.orientation_id = form.cleaned_data['orientation_id']
			obj.blister_id = form.cleaned_data['blister_id']
			obj.blister_view = form.cleaned_data['blister_view']

			obj.drug_pkg_front = form.cleaned_data['drug_pkg_front']
			obj.drug_bltr_front = form.cleaned_data['drug_bltr_front']
			obj.drug_bltr_back = form.cleaned_data['drug_bltr_back']

			obj.full_pkg_wholesale = form.cleaned_data['full_pkg_wholesale']
			obj.full_pkg_partner_rtl = form.cleaned_data['full_pkg_partner_rtl']
			obj.full_pkg_rtl = form.cleaned_data['full_pkg_rtl']
			obj.unit_price_partner_rtl = form.cleaned_data['unit_price_partner_rtl']
			obj.unit_price_rtl = form.cleaned_data['unit_price_rtl']

			obj.save()

			if obj.brand_id is not None:
				self.context["success"] = "good"
				self.context["form"] = CapsuleForm()
			else:
				self.context["success"] = "bad"
				self.context["form"] = form

			return render(  request, self.url , self.context  )








class EditManufacturerView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/edit/addmanufacturer.html'
	context = {}

	def get(self, request, *args, **kwargs):
		self.context["success"] = "x"
		
		if "id" not in kwargs:
			return redirect("view_manufacturer")
		else:
			try:
				man = Manufacturer.objects.get(pk=kwargs["id"])
			except Manufacturer.DoesNotExist:
				return redirect("view_manufacturer")

			
			self.context["form"] = ManufacturerForm(model_to_dict(man))
			return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = ManufacturerForm(request.POST)

		if "id" not in kwargs:
			return redirect("view_manufacturer")
		else:
			try:
				man = Manufacturer.objects.get(pk=kwargs["id"])
			except Manufacturer.DoesNotExist:
				return redirect("view_manufacturer")

			if form.is_valid():
			# process form data
				man.man_name = form.cleaned_data['man_name']
				man.save()

				if man.manufacturer_id is not None:
					self.context["success"] = "good"
					self.context["form"] = ManufacturerForm(model_to_dict(man))
				else:
					self.context["success"] = "bad"
					self.context["form"] = form

				return render(  request, self.url , self.context  )


class EditDrugView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/edit/adddrug.html'
	context = {}

	def get(self, request, *args, **kwargs):
		self.context["success"] = "x"

		if "id" not in kwargs:
			return redirect("view_drugs")
		else:
			try:
				man = Drugs.objects.get(pk=kwargs["id"])
			except Drugs.DoesNotExist:
				return redirect("view_drugs")

			self.context["form"] = DrugForm(model_to_dict(man))
			return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = DrugForm(request.POST)
		if "id" not in kwargs:
			return redirect("view_drugs")
		else:
			try:
				man = Drugs.objects.get(pk=kwargs["id"])
			except Drugs.DoesNotExist:
				return redirect("view_drugs")

			if form.is_valid():
			# process form data
				man.drug_name = form.cleaned_data['drug_name']
				man.save()

				if man.drug_id is not None:
					self.context["success"] = "good"
					self.context["form"] = DrugForm(model_to_dict(man))
				else:
					self.context["success"] = "bad"
					self.context["form"] = form

				return render(  request, self.url , self.context  )


class EditBodyPartView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	context = {}
	url = 'admin/pages/edit/addbodypart.html'

	def get(self, request, *args, **kwargs):
		self.context["success"] = "x"

		if "id" not in kwargs:
			return redirect("view_bodypart")
		else:
			try:
				man = BodyPart.objects.get(pk=kwargs["id"])
			except BodyPart.DoesNotExist:
				return redirect("view_bodypart")

			self.context["form"] = BodyPartForm(model_to_dict(man))
			return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		form = BodyPartForm(request.POST)

		if "id" not in kwargs:
			return redirect("view_bodypart")
		else:
			try:
				man = BodyPart.objects.get(pk=kwargs["id"])
			except BodyPart.DoesNotExist:
				return redirect("view_bodypart")

			if form.is_valid():
			# process form data
				man.body_part = form.cleaned_data['body_part']
				man.save()

				if man.part_id is not None:
					self.context["success"] = "good"
					self.context["form"] = BodyPartForm(model_to_dict(man))
				else:
					self.context["success"] = "bad"
					self.context["form"] = BodyPartForm(request.POST)

				return render(  request, self.url , self.context  )


class EditSymptomView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/edit/addsymptom.html'
	context = { "parts" : BodyPart.objects.all()  }

	def get(self, request, *args, **kwargs):
		#symptoms = Symptom.objects.select_related('part_id')
		#symptoms = symptoms.query

		self.context["success"] = "x"

		if "id" not in kwargs:
			return redirect("view_symptom")
		else:
			try:
				man = Symptom.objects.get(pk=kwargs["id"])
			except Symptom.DoesNotExist:
				return redirect("view_symptom")
			
			self.context["form"] = SymptomForm(model_to_dict(man))
			return render(  request, self.url , self.context  )


	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#symptoms = Symptom.objects.select_related('part_id')
		#symptoms = symptoms.query

		form = SymptomForm(request.POST)

		if "id" not in kwargs:
			return redirect("view_symptom")
		else:
			try:
				man = Symptom.objects.get(pk=kwargs["id"])
			except Symptom.DoesNotExist:
				return redirect("view_symptom")

			if form.is_valid():
			# process form data
				man.symptom = form.cleaned_data['symptom']
				man.part_id = form.cleaned_data['part_id']
				man.description = form.cleaned_data['description']
				man.save()

				if man.symptom_id is not None:
					self.context["success"] = "good"
					self.context["form"] = SymptomForm(model_to_dict(man))
				else:
					self.context["success"] = "bad"
					self.context["form"] = form

				return render(  request, self.url , self.context  )


class EditSymptomTreatmentView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/edit/addsymptomcure.html'
	context = { "symptoms" : Symptom.objects.all(), "brands" : Brand.objects.all() }

	def get(self, request, *args, **kwargs):
		#cures = SymptomTreatment.objects.select_related('symptom_id', 'brand_id')
		#cures = cures.query

		self.context["success"] = "x"

		if "id" not in kwargs:
			return redirect("view_symptom_cures")
		else:
			try:
				man = SymptomTreatment.objects.get(pk=kwargs["id"])
			except SymptomTreatment.DoesNotExist:
				return redirect("view_symptom_cures")

			self.context["form"] = SymptomTreatmentForm(model_to_dict(man))
			return render(  request, self.url , self.context  )


	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#cures = SymptomTreatment.objects.select_related('symptom_id', 'brand_id')
		#cures = cures.query

		form = SymptomTreatmentForm(request.POST)

		if "id" not in kwargs:
			return redirect("view_symptom_cures")
		else:
			try:
				man = SymptomTreatment.objects.get(pk=kwargs["id"])
			except SymptomTreatment.DoesNotExist:
				return redirect("view_symptom_cures")

			if form.is_valid():
			# process form data
				man.symptom_id = form.cleaned_data['symptom_id']
				man.brand_id = form.cleaned_data['brand_id']
				man.save()

				if man.treatment_id is not None:
					self.context["success"] = "good"
					self.context["form"] = SymptomTreatmentForm(model_to_dict(man))
				else:
					self.context["success"] = "bad"
					self.context["form"] = form

				return render(  request, self.url , self.context  )


class EditAdministrationInstructionView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/edit/admininstruction.html'
	context = { "drugs" : Drugs.objects.all() }

	def get(self, request, *args, **kwargs):
		#cures = AdminInstruction.objects.select_related('drug_id')
		#cures = cures.query

		self.context["success"] = "x"

		if "id" not in kwargs:
			return redirect("view_administration_instructions")
		else:
			try:
				man = AdminInstruction.objects.get(pk=kwargs["id"])
			except AdminInstruction.DoesNotExist:
				return redirect("view_administration_instructions")
			
			self.context["form"] = AdministrationInstructionForm(model_to_dict(man))
			return render(  request, self.url , self.context  )

	def post(self, request, *args, **kwargs):
		#request.POST.get('field_name')
		#cures = AdminInstruction.objects.select_related('drug_id')
		#cures = cures.query

		form = AdministrationInstructionForm(request.POST)
		
		if "id" not in kwargs:
			return redirect("view_administration_instructions")
		else:
			try:
				man = AdminInstruction.objects.get(pk=kwargs["id"])
			except AdminInstruction.DoesNotExist:
				return redirect("view_administration_instructions")

			if form.is_valid():
			# process form data
				man.drug_id = form.cleaned_data['drug_id']
				man.instructions = form.cleaned_data['instructions']
				man.save()

				if man.instruction_id is not None:
					self.context["success"] = "good"
					self.context["form"] = AdministrationInstructionForm(model_to_dict(man))
				else:
					self.context["success"] = "bad"
					self.context["form"] = form

				return render(  request, self.url , self.context  )


class EditCapsulesView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	url = 'admin/pages/edit/addcapsule.html'
	context = {}

	def get(self, request, *args, **kwargs):

		self.context["success"] = "x"

		if "id" not in kwargs:
			return redirect("view_capsules")
		else:
			try:
				man = CapsuleBrand.objects.get(pk=kwargs["id"])
			except CapsuleBrand.DoesNotExist:
				return redirect("view_capsules")

			another_man = model_to_dict(man)
			another_man["noIndColor1"] = Colors.objects.get(  pk=man.color1  ).color
			another_man["noIndColor2"] = Colors.objects.get(  pk=man.color2  ).color

			self.context["form"] = CapsuleForm(  another_man  )
			self.context["blister_view"] = man.blister_view

			return render(  request, self.url , self.context  )


	def post(self, request, *args, **kwargs):

		form = CapsuleForm(request.POST)
		if form.is_valid():
		# process form data

			if "id" not in kwargs:
				return redirect("view_capsules")
			else:
				try:
					man = CapsuleBrand.objects.get(pk=kwargs["id"])
				except CapsuleBrand.DoesNotExist:
					return redirect("view_capsules")

				man.drug_id = form.cleaned_data['drug_id']
				man.brand_name = form.cleaned_data['brand_name']
				man.manufacturer_id = form.cleaned_data['manufacturer_id']
				man.country_id = form.cleaned_data['country_id']
				man.nocpsonblister = form.cleaned_data['nocpsonblister']
				man.color1 = Colors.objects.get(  color=form.cleaned_data['noIndColor1']  ).color_id     
				man.color2 = Colors.objects.get(  color=form.cleaned_data['noIndColor2']  ).color_id
				man.orientation_id = form.cleaned_data['orientation_id']
				man.blister_id = form.cleaned_data['blister_id']
				man.blister_view = form.cleaned_data['blister_view']

				man.drug_pkg_front = form.cleaned_data['drug_pkg_front']
				man.drug_bltr_front = form.cleaned_data['drug_bltr_front']
				man.drug_bltr_back = form.cleaned_data['drug_bltr_back']

				man.full_pkg_wholesale = form.cleaned_data['full_pkg_wholesale']
				man.full_pkg_partner_rtl = form.cleaned_data['full_pkg_partner_rtl']
				man.full_pkg_rtl = form.cleaned_data['full_pkg_rtl']
				man.unit_price_partner_rtl = form.cleaned_data['unit_price_partner_rtl']
				man.unit_price_rtl = form.cleaned_data['unit_price_rtl']

				man.save()

				if man.brand_id is not None:
					self.context["success"] = "good"
					self.context["form"] = form
				else:
					self.context["success"] = "bad"
					self.context["form"] = form

				self.context["blister_view"] = man.blister_view
				return render(  request, self.url , self.context  )











class DeleteManufacturerView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_manufacturer")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_manufacturer")
		
		request.session['success_delete'] = "good"

		try:
			man = Manufacturer.objects.get(pk=request.POST["id"]).delete()
		except:
			request.session['success_delete'] = "bad"
			return redirect("view_manufacturer")

		return redirect("view_manufacturer")


class DeleteDrugView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_drugs")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_drugs")
		
		request.session['success_delete'] = "good"

		try:
			man = Drugs.objects.get(pk=request.POST["id"]).delete()
		except:
			request.session['success_delete'] = "bad"

		return redirect("view_drugs")


class DeleteBodyPartView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_bodypart")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_bodypart")
		
		request.session['success_delete'] = "good"

		try:
			man = BodyPart.objects.get(pk=request.POST["id"]).delete()
		except:
			request.session['success_delete'] = "bad"

		return redirect("view_bodypart")


class DeleteSymptomView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_symptom")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_symptom")
		
		request.session['success_delete'] = "good"

		try:
			man = Symptom.objects.get(pk=request.POST["id"])
		except:
			request.session['success_delete'] = "bad"

		return redirect("view_symptom")


class DeleteSymptomTreatmentView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_symptom_cures")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_symptom_cures")
		
		request.session['success_delete'] = "good"

		try:
			man = SymptomTreatment.objects.get(pk=request.POST["id"]).delete()
		except:
			request.session['success_delete'] = "bad"

		return redirect("view_symptom_cures")


class DeleteAdministrationInstructionView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_administration_instructions")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_administration_instructions")
		
		request.session['success_delete'] = "good"

		try:
			man = AdminInstruction.objects.get(pk=request.POST["id"]).delete()
		except:
			request.session['success_delete'] = "bad"

		return redirect("view_administration_instructions")


class DeleteCapsulesView(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return redirect("view_capsules")

	def post(self, request, *args, **kwargs):

		if request.POST["id"] is None:
			return redirect("view_capsules")
		
		request.session['success_delete'] = "good"

		try:
			man = CapsuleBrand.objects.get(pk=request.POST["id"]).delete()
		except:
			request.session['success_delete'] = "bad"

		return redirect("view_capsules")








class GetDiseasesGivenSymptoms(LoginRequiredMixin, View):

	login_url = '/admin/login/'

	def get(self, request, *args, **kwargs):
		return JsonResponse()

	def post(self, request, *args, **kwargs):
		
		if request.POST.getlist("symptoms[]") is None or len(request.POST.getlist("symptoms[]")) == 0:
			return JsonResponse({})

		symptoms = request.POST.getlist("symptoms[]")
		query = "";
		count = 0

		for symptom in symptoms:
			if count > 0:
				query = query + " and "

			query = query + "( symptoms='"+str(symptom)+"' OR symptoms LIKE '%%"+str(symptom)+",%%' OR symptoms LIKE '%%,"+str(symptom)+"%%' OR symptoms LIKE '%%,"+str(symptom)+",%%' ) "
			count = count + 1

		diseases = Disease.objects.extra(where=[ query ]) 
		return JsonResponse(  dict(  diseases=list(  diseases.values("disease")  )  )  )





